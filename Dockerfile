FROM debian:stable-slim

MAINTAINER Martino Visintin

RUN apt-get update && \
	apt-get -y install make pandoc context && \
	apt-get clean
