all: docker_build docker_push

docker_build:
	docker build --tag pandocker:latest .

docker_push:
	docker push pandocker:latest
